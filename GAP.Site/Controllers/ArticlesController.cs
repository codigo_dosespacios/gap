﻿using GAP.Site.Entities;
using GAP.Site.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace GAP.Site.Controllers
{
	public class ArticlesController : Controller
	{
		private ArticlesDataManager data = new ArticlesDataManager();

		public ArticlesController()
		{
			ViewData[@"StoresList"] = GeSelectListStores();
			ViewBag.Stores_List = GeSelectListStores();
			((List<SelectListItem>)ViewData[@"StoresList"]).RemoveAt(0);
		}

		private List<SelectListItem> GeSelectListStores()
		{
			List<SelectListItem> _Lista = new List<SelectListItem>();
			try
			{
				_Lista.Add(new SelectListItem() { Text = @"All", Value = @"0" });
				var lista = data.GetStores().Stores;
				foreach (var item in lista)
					_Lista.Add(new SelectListItem() { Text = item.name, Value = item.id.ToString() });
			}
			catch (Exception ex)
			{
				ShowMessage(ex.Message);
			}
			return _Lista;
		}

		public ActionResult Index(int? id)
		{
			try
			{

				if (id == null)
					return View(data.GetAll());
				else
					return View(data.GetArticlesByStore(Convert.ToInt32(id)));
			}
			catch (Exception ex)
			{
				ShowMessage(ex.Message);
			}
			return View();
		}

		public ActionResult Create()
		{
			return View();
		}

		[HttpPost]
		public ActionResult Create(Articles Article)
		{
			try
			{
				var result = data.Add(Article); // Article mesaage in Log
				return RedirectToAction(@"Index");
			}
			catch (Exception ex)
			{
				ShowMessage(ex.Message);
			}
			return View(Article);
		}

		public ActionResult Edit(int id)
		{
			try
			{
				var result = data.GetById(id);
				ViewBag.store_id = new SelectList(data.GetStores().Stores, @"id", @"name", result.store_id);
				TempData[@"store_id"] = result.store_id;
				return View(result);
			}
			catch (Exception ex)
			{

				ShowMessage(ex.Message);
			}
			return View();
		}

		[HttpPost]
		public ActionResult Edit(int id, Articles Article)
		{
			try
			{
				var result = data.Update(id, Article); // Article mesaage in Log
				return RedirectToAction(@"Index");
			}
			catch (Exception ex)
			{
				ShowMessage(ex.Message);
			}
			return View(Article);
		}

		public JsonResult Delete(int id)
		{
			try
			{
				data.Delete(id);
			}
			catch (Exception ex)
			{
				ShowMessage(ex.Message);
			}
			return Json(data.GetAll(), JsonRequestBehavior.AllowGet);
		}

		private void ShowMessage(string JsonMsg)
		{
			var mensaje = JsonConvert.DeserializeObject<ResponseRequest>(JsonMsg);
			ViewBag.error_code = mensaje.error_code;
			ViewBag.error_msg = mensaje.error_msg;
			ViewBag.LaunchModalPopup = @"<script type='text/javascript'>$(document).ready(function(){ $('#MessageModal').modal('show'); });</script>";
		}
	}
}