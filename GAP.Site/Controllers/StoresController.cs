﻿using GAP.Site.Entities;
using GAP.Site.Models;
using Newtonsoft.Json;
using System;
using System.Web.Mvc;

namespace GAP.Site.Controllers
{
	public class StoresController : Controller
	{
		private StoresDataManager data = new StoresDataManager();
		public ActionResult Index()
		{
			try
			{
				return View(data.GetAll());
			}
			catch (Exception ex)
			{
				ShowMessage(ex.Message);
			}
			return View();
		}

		public ActionResult Create()
		{
			return View();
		}

		[HttpPost]
		public ActionResult Create(Stores Store)
		{
			try
			{
				var result = data.Add(Store); // Store mesaage in Log
				return RedirectToAction(@"Index");
			}
			catch (Exception ex)
			{
				ShowMessage(ex.Message);
			}
			return View(Store);
		}

		public ActionResult Edit(int id)
		{
			try
			{
				return View(data.GetById(id));
			}
			catch (Exception ex)
			{

				ShowMessage(ex.Message);
			}
			return View();
		}

		[HttpPost]
		public ActionResult Edit(int id, Stores Store)
		{
			try
			{
				var result = data.Update(id, Store); // Store mesaage in Log
				return RedirectToAction(@"Index");
			}
			catch (Exception ex)
			{
				ShowMessage(ex.Message);
			}
			return View(Store);
		}

		public JsonResult Delete(int id)
		{
			try
			{
				data.Delete(id);
			}
			catch (Exception ex)
			{
				ShowMessage(ex.Message);
			}
			return Json(data.GetAll(), JsonRequestBehavior.AllowGet);
		}

		private void ShowMessage(string JsonMsg)
		{
			var mensaje = JsonConvert.DeserializeObject<ResponseRequest>(JsonMsg);
			ViewBag.error_code = mensaje.error_code;
			ViewBag.error_msg = mensaje.error_msg;
			ViewBag.LaunchModalPopup = @"<script type='text/javascript'>$(document).ready(function(){ $('#MessageModal').modal('show'); });</script>";
		}
	}
}
