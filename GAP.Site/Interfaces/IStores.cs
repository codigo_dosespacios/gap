﻿using GAP.Site.Entities;

namespace GAP.Site.Interfaces
{
	public interface IStores
	{
		string Add(Stores Store);
		string Delete(int id);
		StoresList GetAll();
		Stores GetById(int id);
		string Update(int id, Stores Store);
	}
}
