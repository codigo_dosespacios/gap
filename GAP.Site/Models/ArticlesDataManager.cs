﻿using GAP.Site.Entities;
using GAP.Site.Interfaces;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;

namespace GAP.Site.Models
{
	public class ArticlesDataManager : IArticles
	{
		public StoresList GetStores()
		{
			RestRequest request = SetHeaderRequest(@"GET");
			var client = new RestClient(@"http://localhost:27336/services/stores");
			IRestResponse response = client.Execute(request);

			var jsonConverted = Satinize(response.Content);

			if (jsonConverted.IndexOf(@"Message") > -1)
				throw new Exception(JsonConvert.SerializeObject(new ResponseRequest() { error_code = 401, error_msg = @"Error de Autenticacion.", success = false }));

			return JsonConvert.DeserializeObject<StoresList>(jsonConverted);
		}

		private static RestRequest SetHeaderRequest(string method)
		{
			Method Method_Request = (Method)Enum.Parse(typeof(Method), method);
			RestRequest request = new RestRequest(Method_Request);
			request.AddHeader(@"Content-type", @"application/json; charset=utf-8");
			string encoded = Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes("my_user:my_password"));
			request.AddHeader(@"Authorization", @"Basic " + encoded);
			request.RequestFormat = DataFormat.Json;
			return request;
		}

		private static string Satinize(string json)
		{
			var jsonTemp = json.Replace(@"\\\", @"").Replace(@"\", @"");
			var jsonConverted = jsonTemp.Substring(1, jsonTemp.Length - 3) + "}";
			return jsonConverted;
		}

		public string Add(Articles Article)
		{
			RestRequest request = SetHeaderRequest(@"POST");
			var client = new RestClient(@"http://localhost:27336/services/Articles/");
			var ObjArticle = new Articles { id = Article.id, name = Article.name, description = Article.description, price = Article.price, store_id = Article.store_id, total_in_shelf = Article.total_in_shelf, total_in_vault = Article.total_in_vault };
			var jsonStr = JsonConvert.SerializeObject(ObjArticle);
			request.AddParameter(@"application/json; charset=utf-8", jsonStr, ParameterType.RequestBody);
			IRestResponse response = client.Execute(request);

			var json = response.Content.Replace(@"\\\", "").Replace(@"\", "");
			var jsonConverted = json.Substring(1, json.Length - 3) + @"}";

			if (jsonConverted.IndexOf(@"Message") > -1)
				throw new Exception(JsonConvert.SerializeObject(new ResponseRequest() { error_code = 401, error_msg = @"Error de Autenticacion.", success = false }));

			if (jsonConverted.IndexOf(@":false") > -1)
				throw new Exception(jsonConverted);

			return jsonConverted;
		}

		public string Delete(int id)
		{
			RestRequest request = SetHeaderRequest(@"DELETE");
			var client = new RestClient(@"http://localhost:27336/services/Articles/" + id);
			IRestResponse response = client.Execute(request);

			var json = response.Content.Replace(@"\\\", "").Replace(@"\", @"");
			var jsonConverted = json.Substring(1, json.Length - 3) + @"}";

			if (jsonConverted.IndexOf(@"Message") > -1)
				throw new Exception(JsonConvert.SerializeObject(new ResponseRequest() { error_code = 401, error_msg = @"Error de Autenticacion.", success = false }));

			if (jsonConverted.IndexOf(@":false") > -1)
				throw new Exception(jsonConverted);

			return jsonConverted;
		}

		public ArticlesList GetAll()
		{
			RestRequest request = SetHeaderRequest(@"GET");
			var client = new RestClient(@"http://localhost:27336/services/Articles");
			IRestResponse response = client.Execute(request);

			var json = response.Content.Replace(@"\\\", @"").Replace(@"\", @"");
			var jsonConverted = json.Substring(1, json.Length - 3) + "}";

			if (jsonConverted.IndexOf(@"Message") > -1)
				throw new Exception(JsonConvert.SerializeObject(new ResponseRequest() { error_code = 401, error_msg = @"Error de Autenticacion.", success = false }));

			return JsonConvert.DeserializeObject<ArticlesList>(jsonConverted);
		}

		public ArticlesList GetArticlesByStore(int id)
		{
			RestRequest request = SetHeaderRequest(@"GET");
			var client = new RestClient(@"http://localhost:27336/services/Articles/Stores/" + id);
			IRestResponse response = client.Execute(request);

			var json = response.Content.Replace(@"\\\", @"").Replace(@"\", @"");
			var jsonConverted = json.Substring(1, json.Length - 3) + "}";

			if (jsonConverted.IndexOf(@"Message") > -1)
				throw new Exception(JsonConvert.SerializeObject(new ResponseRequest() { error_code = 401, error_msg = @"Error de Autenticacion.", success = false }));

			return JsonConvert.DeserializeObject<ArticlesList>(jsonConverted);
		}

		public Articles GetById(int id)
		{
			RestRequest request = SetHeaderRequest(@"GET");
			var client = new RestClient(@"http://localhost:27336/services/Articles/" + id);
			IRestResponse response = client.Execute(request);

			var json = response.Content.Replace(@"\\\", "").Replace(@"\", @"");
			var jsonConverted = json.Substring(1, json.Length - 3) + @"}";
			var objDeserialized = JsonConvert.DeserializeObject<Articles>(jsonConverted);

			if (jsonConverted.IndexOf(@"Message") > -1)
				throw new Exception(JsonConvert.SerializeObject(new ResponseRequest() { error_code = 401, error_msg = @"Error de Autenticacion.", success = false }));

			if (jsonConverted.IndexOf(@":false") > -1)
				throw new Exception(jsonConverted);

			return JsonConvert.DeserializeObject<Articles>(jsonConverted);
		}

		public string Update(int id, Articles Article)
		{
			var client = new RestClient(@"http://localhost:27336/services/Articles/" + id);
			Method Method_Request = (Method)Enum.Parse(typeof(Method), @"PUT");
			RestRequest request = new RestRequest(Method_Request);
			request.AddHeader(@"Content-type", @"application/json; charset=utf-8");
			request.RequestFormat = DataFormat.Json;

			var ObjArticle = new Articles { id = Article.id, name = Article.name, description = Article.description, price = Article.price, store_id = Article.store_id, total_in_shelf = Article.total_in_shelf, total_in_vault = Article.total_in_vault };
			var jsonStr = JsonConvert.SerializeObject(ObjArticle);
			request.AddParameter(@"application/json; charset=utf-8", jsonStr, ParameterType.RequestBody);
			IRestResponse response = client.Execute(request);

			var json = response.Content.Replace(@"\\\", "").Replace(@"\", @"");
			var jsonConverted = json.Substring(1, json.Length - 3) + @"}";

			if (jsonConverted.IndexOf(@":false") > -1)
				throw new Exception(jsonConverted);

			return jsonConverted;
		}
	}
}