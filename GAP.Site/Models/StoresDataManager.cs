﻿using GAP.Site.Entities;
using GAP.Site.Interfaces;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Web.Mvc;

namespace GAP.Site.Models
{
	public class StoresDataManager : IStores
	{
		public string Add(Stores Store)
		{
			RestRequest request = SetHeaderRequest(@"POST");
			var client = new RestClient(@"http://localhost:27336/services/stores/");
			var ObjStore = new Stores { name = Store.name, address = Store.address };
			var jsonStr = JsonConvert.SerializeObject(ObjStore);
			request.AddParameter(@"application/json; charset=utf-8", jsonStr, ParameterType.RequestBody);
			IRestResponse response = client.Execute(request);

			var json = response.Content.Replace(@"\\\", "").Replace(@"\", "");
			var jsonConverted = json.Substring(1, json.Length - 3) + @"}";

			if (jsonConverted.IndexOf(@"Message") > -1)
				throw new Exception(JsonConvert.SerializeObject(new ResponseRequest() { error_code = 401, error_msg = @"Error de Autenticacion.", success = false }));

			if (jsonConverted.IndexOf(@":false") > -1)
				throw new Exception(jsonConverted);

			return jsonConverted;
		}

		public string Delete(int id)
		{
			RestRequest request = SetHeaderRequest(@"DELETE");
			var client = new RestClient(@"http://localhost:27336/services/stores/" + id);
			IRestResponse response = client.Execute(request);

			var json = response.Content.Replace(@"\\\", "").Replace(@"\", @"");
			var jsonConverted = json.Substring(1, json.Length - 3) + @"}";

			if (jsonConverted.IndexOf(@"Message") > -1)
				throw new Exception(JsonConvert.SerializeObject(new ResponseRequest() { error_code = 401, error_msg = @"Error de Autenticacion.", success = false }));

			if (jsonConverted.IndexOf(@":false") > -1)
				throw new Exception(jsonConverted);

			return jsonConverted;
		}

		[Authorize]
		public StoresList GetAll()
		{
			RestRequest request = SetHeaderRequest(@"GET");
			var client = new RestClient(@"http://localhost:27336/services/stores");
			IRestResponse response = client.Execute(request);

			var json = response.Content.Replace(@"\\\", @"").Replace(@"\", @"");
			var jsonConverted = json.Substring(1, json.Length - 3) + "}";

			if (jsonConverted.IndexOf(@"Message") > -1)
				throw new Exception(JsonConvert.SerializeObject(new ResponseRequest() { error_code = 401, error_msg = @"Error de Autenticacion.", success = false }));

			return JsonConvert.DeserializeObject<StoresList>(jsonConverted);
		}

		private static RestRequest SetHeaderRequest(string method)
		{
			Method Method_Request = (Method)Enum.Parse(typeof(Method), method);
			RestRequest request = new RestRequest(Method_Request);
			request.AddHeader(@"Content-type", @"application/json; charset=utf-8");
			string encoded = Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes("my_user:my_password"));
			request.AddHeader(@"Authorization", @"Basic " + encoded);
			request.RequestFormat = DataFormat.Json;
			return request;
		}

		public Stores GetById(int id)
		{
			RestRequest request = SetHeaderRequest(@"GET");
			var client = new RestClient(@"http://localhost:27336/services/stores/" + id);
			IRestResponse response = client.Execute(request);

			var json = response.Content.Replace(@"\\\", "").Replace(@"\", @"");
			var jsonConverted = json.Substring(1, json.Length - 3) + @"}";
			var objDeserialized = JsonConvert.DeserializeObject<Stores>(jsonConverted);

			if (jsonConverted.IndexOf(@"Message") > -1)
				throw new Exception(JsonConvert.SerializeObject(new ResponseRequest() { error_code = 401, error_msg = @"Error de Autenticacion.", success = false }));

			if (jsonConverted.IndexOf(@":false") > -1)
				throw new Exception(jsonConverted);

			return JsonConvert.DeserializeObject<Stores>(jsonConverted);
		}

		public string Update(int id, Stores Store)
		{
			RestRequest request = SetHeaderRequest(@"PUT");
			var client = new RestClient(@"http://localhost:27336/services/stores/" + id);
			var ObjStore = new Stores { id = id, name = Store.name, address = Store.address };
			var jsonStr = JsonConvert.SerializeObject(ObjStore);
			request.AddParameter(@"application/json; charset=utf-8", jsonStr, ParameterType.RequestBody);
			IRestResponse response = client.Execute(request);

			var json = response.Content.Replace(@"\\\", "").Replace(@"\", @"");
			var jsonConverted = json.Substring(1, json.Length - 3) + @"}";

			if (jsonConverted.IndexOf(@"Message") > -1)
				throw new Exception(JsonConvert.SerializeObject(new ResponseRequest() { error_code = 401, error_msg = @"Error de Autenticacion.", success = false }));

			if (jsonConverted.IndexOf(@":false") > -1)
				throw new Exception(jsonConverted);

			return jsonConverted;
		}
	}
}