﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GAP.Site.Entities
{
	public class ArticlesList
	{
		public List<Articles> Articles { get; set; }
		public bool success { get; set; }
		public int total_elements { get; set; }
	}
}