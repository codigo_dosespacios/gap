﻿using System.Web.Http;

namespace GAP.Service
{
	public static class WebApiConfig
	{
		public static void Register(HttpConfiguration config)
		{
			// Configuración y servicios de API web

			// Rutas de API web
			config.MapHttpAttributeRoutes();

			config.Routes.MapHttpRoute(
				name: "DefaultApi",
				routeTemplate: "services/{controller}/{id}",
				defaults: new { id = RouteParameter.Optional }
			);
		}
	}
}
