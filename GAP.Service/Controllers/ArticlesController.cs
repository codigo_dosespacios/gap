﻿using GAP.Service.EF;
using GAP.Service.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using static GAP.Service.Utilities;

namespace GAP.Service.Controllers
{
	[Authorize]
	public class ArticlesController : ApiController
	{
		private GAPEntities db = new GAPEntities();

		[Authorize]
		public IHttpActionResult GetArticles()
		{
			string result = string.Empty;
			HttpStatusCode resultStatus = HttpStatusCode.OK;
			try
			{
				var list = db.Articles;
				ArticlesList Lista = new ArticlesList();
				Lista.Articles = new List<Articles>();
				foreach (var item in list)
					Lista.Articles.Add(new Articles() { id = item.id, name = item.name, description = item.description, price = item.price, store_id = item.store_id, total_in_shelf = item.total_in_shelf, total_in_vault = item.total_in_vault });

				Lista.success = true;
				Lista.total_elements = list.Count();
				result = JsonConvert.SerializeObject(Lista);
			}
			catch (Exception ex)
			{
				result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 500, error_msg = ex.Message, success = false });
				resultStatus = HttpStatusCode.InternalServerError;
			}
			return Content(resultStatus, result);
		}

		[ResponseType(typeof(Articles))]
		public IHttpActionResult GetArticle(int id)
		{
			string result = string.Empty;
			HttpStatusCode resultStatus = HttpStatusCode.OK;
			try
			{
				var data = db.Articles.Find(id);
				if (data == null)
				{
					result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 404, error_msg = @"Record not found", success = false });
					resultStatus = HttpStatusCode.NotFound;
				}
				else
					result = JsonConvert.SerializeObject(new Articles() { id = data.id, name = data.name, description = data.description, price = data.price, store_id = data.store_id, total_in_shelf = data.total_in_shelf, total_in_vault = data.total_in_vault });
			}
			catch (Exception ex)
			{
				result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 500, error_msg = ex.Message, success = false });
				resultStatus = HttpStatusCode.InternalServerError;
			}
			return Content(resultStatus, result);
		}

		//Get Articles By Store
		[Authorize]
		[HttpGet]
		[Route("Services/{Articles}/{Stores}/{id}")]
		[ResponseType(typeof(Articles))]
		public IHttpActionResult GetStores(int id)
		{
			string result = string.Empty;
			HttpStatusCode resultStatus = HttpStatusCode.OK;
			try
			{
				var list = db.Articles.Where(r => r.store_id == id);
				ArticlesList Lista = new ArticlesList();
				Lista.Articles = new List<Articles>();
				foreach (var item in list)
					Lista.Articles.Add(new Articles() { id = item.id, name = item.name, description = item.description, price = item.price, store_id = item.store_id, total_in_shelf = item.total_in_shelf, total_in_vault = item.total_in_vault });

				Lista.success = true;
				Lista.total_elements = list.Count();
				result = JsonConvert.SerializeObject(Lista);
			}
			catch (Exception ex)
			{
				result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 500, error_msg = ex.Message, success = false });
				resultStatus = HttpStatusCode.InternalServerError;
			}
			return Content(resultStatus, result);
		}

		[Authorize]
		[ResponseType(typeof(void))]
		public IHttpActionResult PutArticle(int id, Articles Article)
		{
			string result = string.Empty;
			HttpStatusCode resultStatus = HttpStatusCode.OK;
			if (!ModelState.IsValid)
			{
				result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 400, error_msg = GetModelStateError(ModelState), success = false });
				resultStatus = HttpStatusCode.BadRequest;
			}
			if (id != Article.id)
			{
				result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 404, error_msg = @"Record not found", success = false });
				resultStatus = HttpStatusCode.NotFound;
			}
			try
			{
				Article Art = new Article() { id = Article.id, name = Article.name, description = Article.description, price = Article.price, store_id = Article.store_id, total_in_shelf = Article.total_in_shelf, total_in_vault = Article.total_in_vault };
				db.Entry(Art).State = EntityState.Modified;
				db.SaveChanges();
				result = JsonConvert.SerializeObject(Art);
			}
			catch (Exception ex)
			{
				result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 500, error_msg = ex.Message, success = false });
				resultStatus = HttpStatusCode.InternalServerError;
			}
			return Content(resultStatus, result);
		}

		[Authorize]
		[ResponseType(typeof(Articles))]
		public IHttpActionResult PostArticle(Articles Article)
		{
			string result = string.Empty;
			HttpStatusCode resultStatus = HttpStatusCode.OK;
			if (!ModelState.IsValid)
			{
				result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 400, error_msg = GetModelStateError(ModelState), success = false });
				resultStatus = HttpStatusCode.BadRequest;
			}
			try
			{
				Article art = new Article() { id = Article.id, name = Article.name, description = Article.description, price = Article.price, store_id = Article.store_id, total_in_shelf = Article.total_in_shelf, total_in_vault = Article.total_in_vault };
				db.Articles.Add(art);
				db.SaveChanges();
				result = JsonConvert.SerializeObject(new Article() { id = Article.id, name = Article.name, description = Article.description, price = Article.price, store_id = Article.store_id, total_in_shelf = Article.total_in_shelf, total_in_vault = Article.total_in_vault });
			}
			catch (Exception ex)
			{
				if (ArticleExists(Article.id))
				{
					result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 500, error_msg = @"id duplicated", success = false });
					resultStatus = HttpStatusCode.InternalServerError;
				}
				else
					result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 500, error_msg = ex.Message, success = false });
			}
			return Content(resultStatus, result);
		}

		[Authorize]
		[ResponseType(typeof(Articles))]
		public IHttpActionResult DeleteArticle(int id)
		{
			string result = string.Empty;
			HttpStatusCode resultStatus = HttpStatusCode.OK;
			var data = db.Articles.Find(id);
			if (data == null)
			{
				result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 404, error_msg = @"Record not found", success = false });
				resultStatus = HttpStatusCode.NotFound;
			}
			else
			{
				try
				{
					db.Articles.Remove(data);
					db.SaveChanges();
					result = JsonConvert.SerializeObject(new Articles() { id = data.id, name = data.name, description = data.description, price = data.price, store_id = data.store_id, total_in_shelf = data.total_in_shelf, total_in_vault = data.total_in_vault });
				}
				catch (Exception ex)
				{
					result = JsonConvert.SerializeObject(new ResponseRequest() { error_code = 500, error_msg = ex.Message, success = false });
					resultStatus = HttpStatusCode.InternalServerError;
				}
			}
			return Content(resultStatus, result);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing) db.Dispose();
			base.Dispose(disposing);
		}

		private bool ArticleExists(int id)
		{
			return db.Articles.Count(e => e.id == id) > 0;
		}
	}
}