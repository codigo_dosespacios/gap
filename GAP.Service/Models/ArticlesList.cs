﻿using System.Collections.Generic;

namespace GAP.Service.Models
{
	public class ArticlesList
	{
		public List<Articles> Articles { get; set; }
		public bool success { get; set; }
		public int total_elements { get; set; }
	}
}