﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GAP.Service.Models
{
	public class ResponseRequest
	{
		public bool success { get; set; }
		public int error_code { get; set; }
		public string error_msg { get; set; }
	}
}