﻿namespace GAP.Service.Models
{
	public class Stores
	{
		public int id { get; set; }
		public string name { get; set; }
		public string address { get; set; }
	}
}