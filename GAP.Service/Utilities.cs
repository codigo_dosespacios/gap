﻿using System.Web.Http.ModelBinding;

namespace GAP.Service
{
	public static class Utilities
	{
		public static string GetModelStateError(ModelStateDictionary _ModelState)
		{
			string msgErrors = string.Empty;
			foreach (var modelState in _ModelState)
			{
				foreach (var item in modelState.Value.Errors)
					msgErrors += @"<br> - " + item.ErrorMessage;
			}
			return msgErrors;
		}
	}
}